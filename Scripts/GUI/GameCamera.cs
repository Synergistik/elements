﻿using UnityEngine;
using System.Collections;

public class GameCamera : MonoBehaviour {


	private Transform target;
	private float trackingSpeed;
	private bool zoomOut = false;

//-------------------------------------------------------------------------------------------------------------//

	public void SetTarget(Transform t, float speed = 1000.0f, float delay = 0f, bool zoom = false)
	{
		StartCoroutine (DelayCamera (t, speed, delay, zoom));
	}

//-------------------------------------------------------------------------------------------------------------//

	private IEnumerator DelayCamera (Transform t, float s, float d, bool z)
	{
		yield return new WaitForSeconds(d);
		target = t;
		trackingSpeed = s;
		zoomOut = z;

	}

//-------------------------------------------------------------------------------------------------------------//

	private void LateUpdate(){
		if (target != null){

			float x = IncrementTowards(transform.position.x, target.position.x, trackingSpeed);
			float y = IncrementTowards(transform.position.y, target.position.y+1, trackingSpeed);
			if (zoomOut)
			{
				camera.orthographicSize = IncrementTowards(camera.orthographicSize, 30f, trackingSpeed/4f);
			}
			else if (!zoomOut)
				camera.orthographicSize = 10f;
			transform.position = new Vector3(x,y,transform.position.z);
		}
	}

//-------------------------------------------------------------------------------------------------------------//

	private float IncrementTowards(float n, float target, float a) {
		//fancy method to control camera following/zooming the player
		if (n == target) {
			return n;	
		}
		else {
			float dir = Mathf.Sign(target - n); // must n be increased or decreased to get closer to target
			n += a * Time.deltaTime * dir;
			return (dir == Mathf.Sign(target-n))? n: target; // if n has now passed target then return target, otherwise return n
		}
	}
}
