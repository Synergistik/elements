﻿using UnityEngine;
using System.Collections;

public class UserGUI : MonoBehaviour {

	public GameObject arrowPrefab;
	private GameObject turretObj;
	private Transform arrowObj;

	private PlayerController playerScript;
	private GameManager gm;
	private Texture2D backgroundTexture;
	private GUIText angleText;
	private string power;
	private string distance;
	private string health;
	private string weapon;
	private string location;

//-------------------------------------------------------------------------------------------------------------//

	void Start()
	{
		angleText = GameObject.Find ("AngleText").GetComponent<GUIText> ();
		gm = gameObject.GetComponentInParent<GameManager> ();
		arrowObj = transform.FindChild ("arrow");
		backgroundTexture = new Texture2D (1, 1);
	}
	

//-------------------------------------------------------------------------------------------------------------//

	public void UpdatePlayer(PlayerController playerToSetActive)
	{
		playerScript = playerToSetActive;
	}

//-------------------------------------------------------------------------------------------------------------//

	void UpdateAngle()
	{
		//Updates current angle GUI element
		if (!playerScript.CanShoot)
			angleText.enabled = false; //hide current angle if player just shot
		else 
		{
			angleText.enabled = true;
			angleText.text = playerScript.CurrentTotalAngle.ToString ("0") + "\u00B0";//degrees
		}
	}

//-------------------------------------------------------------------------------------------------------------//

	void UpdateWindText()
	{	//Updates wind force text GUI element
		GUI.Label (new Rect (Screen.width/2-20, Screen.height*(1.6f/8f), 40, 25), (gm.totalWindForce.ToString ("0") + "mph"));
	}
	
//-------------------------------------------------------------------------------------------------------------//

	void SetWindArrow()
	{	//Rotates the arrow sprite according to wind velocity/direction
		//Reset rotation to 0
		arrowObj.rotation = Quaternion.identity;

		//Flip over x-axis based on wind direction
		if (Mathf.Sign (gm.windForce.x) != Mathf.Sign (arrowObj.localScale.x)) 
		{
			Vector3 theScale = arrowObj.localScale ;
			theScale.x *= -1;
			arrowObj.localScale  = theScale;
		}
		//Rotate the arrow based on our force angle and wind direction
		arrowObj.Rotate (Vector3.forward, gm.windAngle * Mathf.Sign (gm.windForce.x));
	}	

//-------------------------------------------------------------------------------------------------------------//

	void UpdateDist()
	{
		//Updates distance traveled GUI element
		distance = "Traveled: " + (playerScript.DistanceTraveled/500.0f).ToString ("0") + "m";
		GUI.Box(new Rect(125, Screen.height-25, 125, 25), distance);
	}
	
//-------------------------------------------------------------------------------------------------------------//

	void UpdateHealth()
	{
		//Updates health GUI element
		health = "Health:" + (playerScript.Health).ToString ("0");
		GUI.Box(new Rect(0, Screen.height-25, 125, 25), health);
	}
	
//-------------------------------------------------------------------------------------------------------------//

	void UpdateShotPower()
	{
		//Updates shot power GUI element
		power = "Power: " + (playerScript.ShotPower).ToString ("0");
		GUI.Box(new Rect(0, Screen.height-76, 100, 50), power);
		DrawQuad (new Rect(100, Screen.height-76, (Screen.width-100f)*(playerScript.ShotPower / playerScript.MaxShotPower), 50));
	}
	
//-------------------------------------------------------------------------------------------------------------//
	
	void UpdateWeapon()
	{
		//Updates current weapon GUI element
		weapon = (playerScript.SelectedWeapon);
		GUI.Box(new Rect(0, Screen.height-103, 100, 25), weapon);
	}
	
//-------------------------------------------------------------------------------------------------------------//
	
	void UpdateLocation()
	{
		//Updates current X,Y location GUI element
		location = "Location: X: " + playerScript.CurrentLocation.x.ToString ("0.00") + "  Y:" + playerScript.CurrentLocation.y.ToString ("0.00");
		GUI.Box(new Rect(Screen.width-175, Screen.height-25, 175, 25), location);
	}

//-------------------------------------------------------------------------------------------------------------//

	void SetBackgroundTextureColor(Color color)
	{
		backgroundTexture.SetPixel (0, 0, color);
		backgroundTexture.Apply ();
	}
	
//-------------------------------------------------------------------------------------------------------------//

	void DrawQuad(Rect position) 
	{
		//Draws the shot power bar
		SetBackgroundTextureColor(Color.red); //make the bar red
		GUI.skin.box.normal.background = backgroundTexture;
		GUI.Box(position, GUIContent.none);
		SetBackgroundTextureColor(Color.black); //reset gui textures back to black
	}
	
//-------------------------------------------------------------------------------------------------------------//
	
	void OnGUI()
	{
		//Called every frame to update the GUI
		SetWindArrow (); //Handles arrow GUI element on new games
		UpdateWindText ();
		UpdateAngle ();
		UpdateDist ();
		UpdateHealth ();
		UpdateShotPower ();
		UpdateWeapon ();
		UpdateLocation ();

		if (GUI.Button (new Rect (0, Screen.height*(0.1f/8.0f), 150, 25), "Reset Level")) 
			gm.StartGame();
		if (GUI.Button (new Rect (0, Screen.height*(0.1f/8.0f)+26, 150, 25), "Lock Camera To Player"))
			gm.LockCamera();
		if (GUI.Button (new Rect (0, Screen.height*(0.1f/8.0f)+52, 150, 25), "Create Practice Target")) 
			gm.MakeTarget(playerScript.CurrentLocation.x, 6f);//Makes a shootable target that can be dragged around in game
		if (GUI.Button (new Rect (0, Screen.height*(0.1f/8.0f)+78, 150, 25), "Two-Player Game"))
			gm.AddPlayerTwo();
	}

//-------------------------------------------------------------------------------------------------------------//

}