﻿using UnityEngine;
using System.Collections;

interface IDestroyable 
{
	void DoDamage(ActiveProjectileStats hitInformation);
}
