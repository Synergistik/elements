﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

	public GameObject playerPrefab;
	public GameObject targetPrefab;
	public Vector2 windForce;
	public float totalWindForce;
	public float windAngle;
	public bool cameraLock = false;

	private GameCamera cam;
	private PlayerController currentPlayer;
	private GameObject[] playerList = new GameObject[1];
	private int currentPlayerTurn = 0;
	private TerrainDestruction2D.TerrainManager terrainManager;

//-------------------------------------------------------------------------------------------------------------//

	void Start () 
	{
		cam = GetComponent<GameCamera> (); //grab camera since it's on the same object
		terrainManager = cam.GetComponent<TerrainDestruction2D.TerrainManager> ();
		StartGame ();
	}
	
//-------------------------------------------------------------------------------------------------------------//
	
	public void PlayerDeath(GameObject PlayerThatDied)
	{	
		for (int i=0; i<playerList.Length; i++) 
		{
			if (playerList[i] == PlayerThatDied)
			{
				SpawnPlayer (new Vector3 (20f*(i+1), 4f, 0), i);
			}
		}
		NextPlayerTurn ();
	}

//-------------------------------------------------------------------------------------------------------------//
	
	public void AddPlayerTwo()
	{	
		ClearWorld ();
		playerList = new GameObject[2];
		StartGame ();
	}
	

//-------------------------------------------------------------------------------------------------------------//

	private void SetActivePlayer(int playerToSetActive)
	{	//Finds the next player in the list and makes it their turn

		currentPlayer = playerList [playerToSetActive].GetComponent<PlayerController> ();
		cam.SetTarget (currentPlayer.transform, 5000f, 0.5f);//Snaps camera to player after short delay
		currentPlayer.enabled = true; 


		UpdateGuiActivePlayer (currentPlayer);
		currentPlayer.CanShoot = true;
	}

//-------------------------------------------------------------------------------------------------------------//

	private void UpdateGuiActivePlayer(PlayerController playerToSetActive)
	{	//Tell GUI which player to display information for.
		cam.GetComponentInChildren<UserGUI> ().UpdatePlayer (playerToSetActive);
	}

//-------------------------------------------------------------------------------------------------------------//
	
	public void NextPlayerTurn()
	{	
		playerList [currentPlayerTurn].GetComponent<PlayerController> ().enabled = false;
		currentPlayerTurn += 1;
		if (currentPlayerTurn == playerList.Length)
			currentPlayerTurn = 0;
		if (Random.value >= 0.9) //10% chance of randomizing the wind on turn change
			SetupWind ();

		SetActivePlayer (currentPlayerTurn);
	}

//-------------------------------------------------------------------------------------------------------------//

	public void StartGame()
	{
		ClearWorld (); //Delete all objects
		SetupWind (); //Randomize X & Y wind force
		CreateLevel (); //Generates new polygon mesh for a new level
		for (int i=0; i<playerList.Length; i++)
			SpawnPlayer (new Vector3 (20f*(i+1), 4f, 0), i); //Spawns the player and sets the camera to follow
		SetActivePlayer (0);
	}

//-------------------------------------------------------------------------------------------------------------//

	public void LockCamera()
	{
		cameraLock = !cameraLock;//Camera follows projectile/locks to player
	}
	
//-------------------------------------------------------------------------------------------------------------//

	void SetupWind()
	{
		windForce = new Vector2 (Random.Range(-10f, 11f), Random.Range(-8f, 9f));
		windAngle = FindForceAngle (windForce);
	}

//-------------------------------------------------------------------------------------------------------------//
	
	float FindForceAngle(Vector2 f)
	{	//Given X and Y forces, computes the overall force and finds the directional angle.
		totalWindForce = Mathf.Sqrt ( (f.x * f.x) + (f.y * f.y) ); //Hypotenuse (Force)
		float angle = Mathf.Asin( (f.y / totalWindForce) ) * (180 / Mathf.PI); //Trig to find the angle
		return angle;
	}

//-------------------------------------------------------------------------------------------------------------//

	public void MakeTarget(float x, float y)
	{
		Instantiate (targetPrefab, new Vector3 (x, y, 0), Quaternion.identity);
	}

//-------------------------------------------------------------------------------------------------------------//

	void ClearWorld()
	{
		foreach (GameObject player in playerList)
			Destroy (player);
		foreach (GameObject terrain in GameObject.FindGameObjectsWithTag ("dTerrain"))
			Destroy (terrain);
		foreach (GameObject target in GameObject.FindGameObjectsWithTag ("Target"))
			Destroy (target);
	}

//-------------------------------------------------------------------------------------------------------------//

	void CreateLevel()
	{
		terrainManager.Reset ();
	}

//-------------------------------------------------------------------------------------------------------------//

	public void SetCamera(Transform target, float speed=1000f, float delay=0f, bool zoom=false)
	{
		cam.SetTarget (target, speed, delay, zoom);
	}

//-------------------------------------------------------------------------------------------------------------//

	void SpawnPlayer (Vector3 spawnPoint, int playerNumber) 
	{	//Make an instance of the player at the start point and set the camera to follow him/her
		GameObject newPlayer = Instantiate (playerPrefab, spawnPoint, Quaternion.identity) as GameObject;
		newPlayer.GetComponent<PlayerController> ().enabled = false;
		playerList[playerNumber] = newPlayer;
	}
//-------------------------------------------------------------------------------------------------------------//
}
