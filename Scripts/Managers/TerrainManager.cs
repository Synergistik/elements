﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Globalization;
using ClipperLib;


namespace TerrainDestruction2D
{
	using Polygon = List<IntPoint>;
	using Polygons = List<List<IntPoint>>;
	using MeshFactory = MeshDestruction.MeshMaker;
	using PolyFactory = PolygonDestruction.PolygonMaker;
	
	public class TerrainManager : MonoBehaviour
	{
		
		private Polygon newDamage = new Polygon();
		private Polygons solution = new Polygons();
		private Polygons explosionPolys = new Polygons();
		private Clipper c = new Clipper ();
		
		private Polygons terrainPaths = new Polygons();
		private List<Mesh> terrainMesh  = new List<Mesh>();
		private List<GameObject> terrainObjects;
		private MeshFilter mF;
		
		public GameObject prefabTerrain;
		public Material groundMaterial;
		
		private float scale = 100f;
		
		//-------------------------------------------------------------------------------------------------------------//
		
		public void Reset()
		{
			terrainPaths = PolyFactory.MakePolygons ();
			terrainMesh = MeshFactory.PathsToMesh (terrainPaths, scale);
			drawTerrain (terrainMesh, "Terrain", groundMaterial, "dTerrain");
			explosionPolys = new Polygons ();
		}
		
		//-------------------------------------------------------------------------------------------------------------//
		
		public void RemoveArea (Vector3 origin, float radius, float damage)
		{	
			c.Clear ();//Clears clipper
			
			//circular polygon representing explosion(precision, origin, radius)
			newDamage = PolyFactory.createCircle (25, new Vector2 (origin.x  * scale, origin.y * scale), radius * scale);
			
			explosionPolys.Add (newDamage);
			c.AddPaths (terrainPaths, PolyType.ptSubject, true);//Give clipper our terrain polygon list
			c.AddPaths (explosionPolys, PolyType.ptClip, true);//Give clipper our explosion polygon list
			solution.Clear ();
			
			bool succeeded = c.Execute (ClipType.ctDifference, solution, PolyFillType.pftNonZero, PolyFillType.pftNonZero);
			if (succeeded)//Could we successfully combine terrain + damage? 
			{
				terrainMesh = new List<Mesh> ();
				foreach (Polygon pg in solution)
					terrainMesh.Add (MeshFactory.CreateMesh (pg, scale));//generate a new mesh and add it to our list to draw
				
				clearTerrain (GameObject.FindGameObjectsWithTag ("dTerrain")); //Destroy old terrain
				drawTerrain (terrainMesh, "Terrain", groundMaterial, "dTerrain"); //Draw new terrain
			}
		}
		
		//-------------------------------------------------------------------------------------------------------------//
		
		void clearTerrain (GameObject[] terrain)
		{
			foreach (GameObject polygon in terrain)
				Destroy (polygon);
		}
		
		void drawTerrain (List<Mesh> t, string s, Material mat, string tag)
		{
			terrainObjects = new List<GameObject> ();
			terrainMesh = new List<Mesh> ();
			foreach (Mesh m in t)
			{
				terrainObjects.Add(drawPolygon (m, mat, s, tag));
				terrainMesh.Add(m);
			}
		}
		
		//-------------------------------------------------------------------------------------------------------------//
		
		GameObject drawPolygon(Mesh m, Material g, string n, string tag)
		{	//Instantiates an object, gives it a mesh & matching collider then returns the object
			GameObject t = Instantiate(prefabTerrain) as GameObject;
			t.name = n;
			t.tag = tag;
			
			PolygonCollider2D b = t.GetComponent<PolygonCollider2D> ();
			
			Vector2[] colliderPts = new Vector2[m.vertices.Length];
			
			for (int i = 0; i<m.vertices.Length; i++) 
			{
				colliderPts[i].x = m.vertices [i].x;
				colliderPts[i].y = m.vertices [i].y;
			}
			b.pathCount = m.vertices.Length;
			b.points = colliderPts;
			t.GetComponent<MeshFilter> ().mesh = m;
			t.GetComponent<MeshRenderer> ().material = g;
			return t;
		}
		
		//-------------------------------------------------------------------------------------------------------------//
		
	}
}