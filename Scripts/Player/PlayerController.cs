﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour, IDestroyable {




	public Rigidbody2D prefabBullet;
	public Rigidbody2D prefabProj;

	private float move;
	private float maxSpeed = 10f;
	
	private float currentTotalAngle = 0.0f;
	private float distanceTraveled = 0.0f;
	private float playerHealth;
	private float shotPower;
	private Vector3 currentLocation;
	private string weaponString = "bullet";
 
	private GameManager gameManager;
	
	private bool facingRight = true;
	private bool isGrounded = false;
	private bool canShoot;

	public Transform barrelPosition;
	private Transform angleIndicator;
	private MeshFilter barrel;
	private float barrelAngle = 0f;
	private float minAngle = 0f;
	private float maxAngle = 90.1f;
	private float maxPower = 500f;

//----------------------------------------------Properties-----------------------------------------------//

	public string SelectedWeapon 
	{
		get { return weaponString; }

		private set 
		{
			weaponString = value;
		}
	}

	public float MaxShotPower
	{
		get { return maxPower; }
	}

	public bool CanShoot
	{
		get { return canShoot; }

		set
		{
			canShoot = value;		
		}
	}

	public float Health 
	{
		get { return playerHealth; }

		private set {
			playerHealth += value; //When you die, you die in real life.
			if (playerHealth <= 0)
			{
				gameManager.PlayerDeath(this.gameObject);
				Destroy(gameObject);
			}
		}
	}


	public float ShotPower
	{
		get { return shotPower; }
		
		private set {
			shotPower = value;
		}
	}


	public Vector3 CurrentLocation
	{
		get { return currentLocation; }
		
		private set {
			currentLocation = value;
		}
	}


	public float DistanceTraveled 
	{
		get { return distanceTraveled; }
		
		private set {
			distanceTraveled = value;
		}
	}

	public float CurrentTotalAngle //This represents barrel angle combined with angle of terrain
	{
		get { return currentTotalAngle; }
		
		private set
		{
			if (value > 270f && value <= 360f) 
				currentTotalAngle = value - 360f;
			else
				currentTotalAngle = value;
		}
	}
		
//-----------------------------------------------Unity-Methods------------------------------------------------//
	
	void Awake () 
	{
		prefabProj = prefabBullet;
		barrel = GetComponentInChildren<MeshFilter> ();
		gameManager = GameObject.Find ("Main Camera").GetComponent<GameManager> ();
		angleIndicator = transform.GetChild (1).GetComponent<Transform> ();
		Health = 100f;
		DistanceTraveled = 0;
		rigidbody2D.centerOfMass = new Vector2 (-0.1f, -2f); //Low center of mass to help keep us grounded
	}


	void FixedUpdate () 
	{
		move = Input.GetAxis ("Horizontal"); //Input
		rigidbody2D.velocity = new Vector2 (move * maxSpeed, rigidbody2D.velocity.y); //Movement

		//Update our GUI variables
		DistanceTraveled += rigidbody2D.velocity.magnitude;
		CurrentLocation = rigidbody2D.transform.position;

		if (move == 0 && isGrounded)
			rigidbody2D.Sleep (); //Stops vehicles from rolling down hills when not moving
		else
			rigidbody2D.WakeUp (); //if moving or not grounded, do physics

		isGrounded = false; //Will trigger back on if we're still touching a collider

		CurrentTotalAngle = barrel.transform.eulerAngles.z; //Adjust display angle for 360 rotation
		if (move > 0 && !facingRight) 
			FlipCharacter (); //Flips the sprite opposite it's current orientation over x-axis
		else if (move < 0 && facingRight)
			FlipCharacter ();
	}


	void OnCollisionStay2D(Collision2D other)
	{ //Called every frame that we remain in contact with a collider
		if (!isGrounded)//Only run if we're not grounded
		{
			if (other.gameObject.tag == "dTerrain")
				isGrounded = true;
		}
	}


	void Update()
	{	
		//Weapon selecting
		if (Input.GetKey ("1"))
			prefabProj = prefabBullet;
		if (Input.anyKey)
			SelectedWeapon = prefabProj.transform.gameObject.name; //GUI Text
		
		//Shooting
		if (CanShoot) 
		{
			if (Input.GetKey ("space")) //Hold down spacebar to add power
					ShotPower += 2f;

			if (Input.GetKeyUp ("space")) { //Release spacebar to fire 
					FireProjectile (prefabProj, barrelPosition, ShotPower * 10f);
					ShotPower = 0;
					CanShoot = false;
			}
		}
		
		if (barrelPosition.localEulerAngles.z > 270f)
			barrelAngle = AdjustCurrentAngleForFlipping ();//if we're facing left
		else
			barrelAngle = barrelPosition.localEulerAngles.z;//if we're facing right
		
		if (barrelPosition.localEulerAngles.z >= minAngle && barrelPosition.localEulerAngles.z < maxAngle)//make sure we're between min/max
		{
			if (Input.GetKey ("w"))//Angle Up
			{
				if (barrelAngle+0.33f < 90f) //Checks math before changing angle to avoid issues
				{
					barrelPosition.Rotate (0,0,0.33f);
				}
			}
			if (Input.GetKey("s")) //Angle Down
			{
				if (barrelAngle-0.33 > 0f) //Checks math before changing angle to avoid issues
				{
					barrelPosition.Rotate (0,0,-0.33f);
				}
			}
		}
		AlignAngleIndicator(); //Sets indicator sprite to barrel angle
	}

//------------------------------------------Player-Methods----------------------------------------------------//

	public void DoDamage (ActiveProjectileStats hitInformation)
	{	
		if (!hitInformation.hitPlayer) //This avoids hitting a player with >1 collider multiple times with the same shot.
			Health = hitInformation.maxDamage;
		hitInformation.hitPlayer = true;

	}

	public void KillPlayer ()
	{	
		Health = -100;
	}

	private void FlipCharacter()
	{	//Flips character left or right 
		facingRight = !facingRight;

		transform.localScale = FlipXScale(transform.localScale); //Flip player
		angleIndicator.transform.localScale = FlipXScale (angleIndicator.transform.localScale); //Flip angle indicator

		float flippedY = 0f;
		if (facingRight)
			flippedY = 0f;
		else if (!facingRight)
			flippedY = 180f;

		barrel.transform.eulerAngles = new Vector3(0, flippedY, 0.1f); //Flip the barrel/shoot position
	}

	private Vector3 FlipXScale(Vector3 itemToFlip)
	{ //Flips given Vector3 to the left/right
		Vector3 theScale = itemToFlip;
		theScale.x *= -1;
		itemToFlip = theScale;
		return itemToFlip;
	}

	private void FireProjectile(Rigidbody2D prefab, Transform pos, float force)
	{
		Vector3 startingFirePosition = new Vector3(barrelPosition.position.x, 
		                                           barrelPosition.position.y, 
		                                           -2);//barrel, -2Z so we don't collide

		Rigidbody2D newProjectileObject = Instantiate(prefab, //Our new projectile gameobject
		                                              startingFirePosition, 
		                                              pos.rotation) as Rigidbody2D; 

		if (!facingRight)//Flipped projectile for flipped player
		{
			newProjectileObject.transform.Rotate (new Vector3(0f, 180f, 0f));
			Vector3 theScale = newProjectileObject.transform.localScale;
			theScale.x *= -1;
			newProjectileObject.transform.localScale = theScale;
		}
		newProjectileObject.rigidbody2D.AddForce(pos.right * force);//Add the shoot power/force
	}


	private float AdjustCurrentAngleForFlipping()
	{	//Adjusts for the fact that flipping the character leads to 360 degree angles
		currentTotalAngle = barrelPosition.localEulerAngles.z - 360f;
		barrelPosition.localEulerAngles = new Vector3 (barrelPosition.localEulerAngles.x, barrelPosition.localEulerAngles.y, Mathf.Abs (currentTotalAngle));
		return currentTotalAngle;
	}


	private void AlignAngleIndicator()
	{	//Aligns indicator sprite with shoot position/angle, taking into account left/right flip
		angleIndicator.localEulerAngles = barrelPosition.localEulerAngles;
		if (barrelPosition.rotation.y > 0.1f || barrelPosition.rotation.y < 0f) 
			angleIndicator.localEulerAngles = -barrelPosition.localEulerAngles;
	}
	
	//-------------------------------------------------------------------------------------------------------------//
}
