﻿using UnityEngine;
using System.Collections;

public class Target : MonoBehaviour, IDestroyable {

	void OnDestroy() 
	{
		//Add Score/Subtract Lives here
	}

//-------------------------------------------------------------------------------------------------------------//

	void OnMouseDrag()
	{
		Vector3 pos = Input.mousePosition;
		gameObject.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(pos.x, pos.y, 10));
	}

//-------------------------------------------------------------------------------------------------------------//

	public void DoDamage (ActiveProjectileStats hitInformation)
	{	
		Destroy (gameObject);
	}
}
