﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Globalization;
using ClipperLib;

namespace MeshDestruction
{
	using Polygon = List<IntPoint>;
	using Polygons = List<List<IntPoint>>;

	public class MeshMaker : MonoBehaviour 
	{

//-------------------------------------------------------------------------------------------------------------//

		public static List<Mesh> PathsToMesh (Polygons p, float scale)
		{
			List<Mesh> tM = new List<Mesh>();
			foreach (Polygon poly in p)
				tM.Add (CreateMesh (poly, scale));
			return tM;
		}

//-------------------------------------------------------------------------------------------------------------//

		public static Mesh CreateMesh(Polygon poly, float scale)
		{
			//Create a new mesh
			Mesh mesh = new Mesh();
	
			//Vertices
			Vector3[] verts = new Vector3[poly.Count]; //vertices are V3s
			Vector2[] verts2D = new Vector2[poly.Count]; //Triangulator takes V2
			
			for (int i=0; i< poly.Count; i++)
			{
				verts[i] = new Vector3((float)poly[i].X/scale, (float)poly[i].Y/scale, 0);
				verts2D[i] = new Vector2((float)poly[i].X/scale, (float)poly[i].Y/scale);
			}
			
			//UVs
			Vector2[] uvs = new Vector2[verts.Length];
			for(int i=0; i<verts.Length; i++)
				uvs[i] = new Vector2(verts[i].x, verts[i].y);
			
			//Triangles
			Triangulator tr = new Triangulator (verts2D);
			int[] tris = tr.Triangulate ();
			
			//Assign data to mesh
			mesh.vertices = verts;
			mesh.uv = uvs;
			mesh.triangles = tris;
			
			//Resizing/reorienting
			mesh.RecalculateNormals();
			mesh.RecalculateBounds();  
			mesh.Optimize();

			mesh.name = "MyMesh";

			return mesh;
		}
	}
}