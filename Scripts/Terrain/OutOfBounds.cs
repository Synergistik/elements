﻿using UnityEngine;
using System.Collections;

public class OutOfBounds : MonoBehaviour {

	void OnCollisionEnter2D (Collision2D other) 
	{
		other.gameObject.BroadcastMessage ("KillPlayer", SendMessageOptions.DontRequireReceiver);
	}
}
