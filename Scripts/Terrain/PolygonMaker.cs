﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Globalization;
using ClipperLib;

namespace PolygonDestruction
{
	using Polygon = List<IntPoint>;
	using Polygons = List<List<IntPoint>>;
	
	public class PolygonMaker : MonoBehaviour 
	{

//-------------------------------------------------------------------------------------------------------------//

		public static Polygons MakePolygons ()
		{	//Makes a big rectangle. Large dimensions because we use a scale, Clipper only deals with ints
			Polygons polyList = new Polygons ();
			Polygon poly = new Polygon ();
			poly.Add (new IntPoint (0, 0));
			poly.Add (new IntPoint (1600, 0));
			poly.Add (new IntPoint (2200, -300));
			poly.Add (new IntPoint (5000, -300));
			poly.Add (new IntPoint (5600, 0));
			poly.Add (new IntPoint (7200, 0));
			poly.Add (new IntPoint (7200, -700));
			poly.Add (new IntPoint (6600, -1000));
			poly.Add (new IntPoint (600, -1000));
			poly.Add (new IntPoint (0, -700));
			polyList.Add (poly);
			return polyList;
		}

//-------------------------------------------------------------------------------------------------------------//

		public static Polygon CreatePolygon (Mesh m, float scale)
		{	//Given a Unity Mesh, returns the scaled polygon for clipper
			Vector3[] v = m.vertices;
			Polygon p = new Polygon ();
			p.Add(new IntPoint(v[0].x*scale, v[0].y*scale));
			return p;
		}

//-------------------------------------------------------------------------------------------------------------//

		public static Polygon createCircle(float precision, Vector2 origin, float radius)
		{	//Creates a circle polygon for Clipper
			float angle = 2f * Mathf.PI / precision;
			Polygon circlePoint = new Polygon();
			for (int i=0; i<precision; i++)
			{
				IntPoint newPt = new IntPoint(origin.x+radius*Mathf.Cos (angle*i), origin.y+radius*Mathf.Sin (angle*i));
				circlePoint.Add(newPt);
				
			}
			return circlePoint;
		}

//-------------------------------------------------------------------------------------------------------------//
		
		public static Polygon createRect(Vector2 origin, float width, float height)
		{	//Creates a rectangle polygon for Clipper
			Polygon rectPoint = new Polygon();
			rectPoint.Add(new IntPoint(origin.x, origin.y));
			rectPoint.Add(new IntPoint(origin.x+width, origin.y));
			rectPoint.Add(new IntPoint(origin.x+width, origin.y-height));
			rectPoint.Add(new IntPoint(origin.x, origin.y-height));
			return rectPoint;
		}
		
//-------------------------------------------------------------------------------------------------------------//
	}
}