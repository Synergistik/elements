﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Globalization;


public class TerrainObject : MonoBehaviour, IDestroyable
{
	private TerrainDestruction2D.TerrainManager terrainManager;

	void Start()
	{
		terrainManager = GameObject.Find ("Main Camera").GetComponent<TerrainDestruction2D.TerrainManager> ();
	}

	public void DoDamage (ActiveProjectileStats hitInformation)
	{	
		terrainManager.RemoveArea(hitInformation.impactOrigin, 
		                          hitInformation.blastRadius, 
		                          hitInformation.maxDamage);
	}
}