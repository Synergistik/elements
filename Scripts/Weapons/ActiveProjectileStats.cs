﻿using UnityEngine;
using System.Collections;

public class ActiveProjectileStats {

	public float blastRadius;
	public Vector3 impactOrigin;
	public float maxDamage;
	public bool hitPlayer;

	public ActiveProjectileStats(Vector3 impactOrigin, float blastRadius, float maxDamage)
	{
		this.blastRadius = blastRadius;
		this.impactOrigin = impactOrigin;
		this.maxDamage = maxDamage;
		hitPlayer = false;
	}

}
