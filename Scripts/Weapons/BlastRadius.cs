﻿using System.Collections;

public static class BlastRadius
{
	public const float Basic = 2.5f;
	public const float BasicEarth = 4;
	public const float MultiEarth = 1;
	public const float Landslide = 20;
}