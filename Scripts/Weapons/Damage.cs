﻿using System.Collections;

public static class Damage
{
	public const float Basic = -10.0f;
	public const float BasicEarth = -5.0f;
	public const float MultiEarth = -2.0f;
	public const float Landslide = -20.0f;
}
