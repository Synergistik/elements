﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

	public GameObject explosionPrefab; //the explosion object, set in the inspector
	private bool explodes = true;

	private GameManager gameManager;

//-------------------------------------------------------------------------------------------------------------//

	void Start()
	{
		gameManager = GameObject.Find ("Main Camera").GetComponent<GameManager> ();
		if (!gameManager.cameraLock)
			gameManager.SetCamera (gameObject.transform, 500f, zoom: true);
	}

//-------------------------------------------------------------------------------------------------------------//

	void OnDestroy() 
	{
		gameManager.NextPlayerTurn ();
	}

//-------------------------------------------------------------------------------------------------------------//

	public virtual void OnCollisionEnter2D (Collision2D other) 
	{	//Handles destruction of terrain/objects/self

		Vector2 collisionOrigin = new Vector2 (gameObject.transform.position.x, gameObject.transform.position.y);

		ActiveProjectileStats myHitInformation = new ActiveProjectileStats (collisionOrigin,
		                                                                    BlastRadius.Basic,
		                                                                    Damage.Basic );
		Collider2D[] hitList = Physics2D.OverlapCircleAll (collisionOrigin, BlastRadius.Basic);
		foreach (Collider2D hitObject in hitList) 
		{
			hitObject.gameObject.BroadcastMessage ("DoDamage", myHitInformation, SendMessageOptions.DontRequireReceiver); //Finds a DoDamage method in any scripts attached to these objects
		}

		if (explodes)
			Explosion();
		Destroy (gameObject);//Destroy projectile
	}

//-------------------------------------------------------------------------------------------------------------//

	void FixedUpdate()
	{	//wind!
		gameObject.rigidbody2D.AddForce (gameManager.windForce);
	}

//-------------------------------------------------------------------------------------------------------------//

	void Explosion()
	{	//Instantiates a GO with an explosion animation at the impact location
		GameObject explodeObj = Instantiate (explosionPrefab, gameObject.transform.position, Quaternion.identity) as GameObject;
		Destroy (explodeObj, 0.72f);//Destroy explosion animation after <1s
	}

//-------------------------------------------------------------------------------------------------------------//
}
