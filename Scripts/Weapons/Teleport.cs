﻿using UnityEngine;
using System.Collections;

public class Teleport : MonoBehaviour {

	private GameManager gm;
	private Transform player;
	private Vector3 teleportTo;

//-------------------------------------------------------------------------------------------------------------//
	
	void Start()
	{
		gm = GameObject.Find ("Main Camera").GetComponent<GameManager> ();
		player = GameObject.FindWithTag ("Player").GetComponent<Transform> ();
		if (!gm.cameraLock)
			gm.SetCamera (gameObject.transform, 50f, zoom: true);
	}
	
//-------------------------------------------------------------------------------------------------------------//

	void OnDestroy() 
	{
		gm.NextPlayerTurn();
	}

//-------------------------------------------------------------------------------------------------------------//

	void OnCollisionEnter2D (Collision2D other) 
	{
		if (other.gameObject.tag == "Boundary")//out of bounds
			Destroy (gameObject);
		else 
		{//if we hit something, teleport the player slightly above that position (so we don't clip thru)
			teleportTo = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y+0.5f, 0); 
			Destroy(gameObject);
			player.position = teleportTo;
			player.rotation = Quaternion.identity; //reset player rotation to 0
			player.Rotate(other.contacts[0].normal); //rotate based on the collider first contacted
		}
	}

//-------------------------------------------------------------------------------------------------------------//

	void FixedUpdate()
	{	//wind!
		gameObject.rigidbody2D.AddForce (gm.windForce);
	}

//-------------------------------------------------------------------------------------------------------------//
}
